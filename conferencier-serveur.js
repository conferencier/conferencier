#!/usr/bin/env node
/*!
programme:gestConferences
auteur: pascal TOLEDO
date de creration: 2013.12.11
date de modification: 2013.12.11
version: 0.1

fichier:gestSalles-srv.js
description:envoie au auditeur les informations d'une conference ou plusieurs conferences et des salles utilisees
dependance:
 - socket.io

*/
var FICHIER_NOM="conferenciers-serveur-complet.js";
var FICHIER_VER='0.2';
var http = require('http');
var fs = require('fs');
var util=require('util');
var HOST='0.0.0.0',PORT=11111;
var GLOBALS=[];
//GLOBALS['socket']=[];
//GLOBALS['ROOTPATH']='/www/git/node.js/conferencier/';


// ---------------------------------------------------------------- //

var colors = require('/usr/local/lib/node_modules/colors');

colors.setTheme({
	silly: 'rainbow',
	input: 'yellow',
	verbose: 'cyan',
	prompt:	 'grey',
	info: 'green',
	data: 'blue',
	help: 'cyan',
	warn: 'yellow',
	debug: 'grey',
	error: 'red'
	});

console.log('******************************************************');
t='demarrage de '+FICHIER_NOM+' ver:'+FICHIER_VER;
console.log(t);
console.log('******************************************************');
// -- creation du serveur http et lancement du routage -- //
//var lanceur=require('/www/sites/graspNet/node.js/graspNet-serveur-content.js');
//var lanceur=require('/www/projets/conferencier/conferenciers-serveur-content.js');
//var server = http.createServer(function (req, res){lanceur.redir(req,res)}):
var server = http.createServer(serveurInit);
server.listen(PORT,HOST);


console.log("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
// -- gestion des salles -- //
var gestSalles=require('./gestConferencier.js').gestSalles;
sallesLoad();
//sallesSave();

// -- gestion des conferences -- //
var gestConferences=require('./gestConferencier.js').gestConferences;
var gestConfsRun=   require('./gestConferencier.js').gestConferencesRun	// -- tableau des pages en cours de lecture dans les conferences avec maitre de conferenciers -- //
confsLoad();
//confsSave();
// --- on surveille les changement du fichier--- //
//http://nodejs.org/docs/latest/api/all.html#all_fs_watchfile_filename_options_listener
/*
fs.watchFile('message.text',{ persistent: true, interval: 5007 }, function (curr, prev){
	console.log('the current mtime is: ' + curr.mtime);
	console.log('the previous mtime was: ' + prev.mtime);
	});
*/

console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^".debug);


// -- Chargement de socket.io -- //
//var io = require('/usr/local/lib/node_modules/socket.io').listen(server);
//console.log('----------------------------'.debug);

function serveurInit(req, res){
	//console.log('req:'+req);
	GLOBALS['req']=req;
	// -- recuperation du get -- //
	var url = require('url');
	var psp = url.parse(req.url, true);
	//console.log('query:'+psp.host);
	//console.log('query:'+psp.query);
	//for(var i in psp.query){console.log('psp.query: '+i+':'+psp.query[i]);}

	// -- routage -- //
	var fileType='text/html';
	var f='./gestSalles-client.html';
	var psp2="?";
	switch(psp.query['loadFile']){
                //case 'index':;break;
                case 'client.js':f='./gestSalles-client.js';break;
                case 'gestConferencier':f='./gestConferencier.js';break;
 		case 'jQuery':f='/www/sites/intersites/lib/tiers/js/jquery/jquery-1.8.1.min.js';break;
		case 'confVIewer':f='./conf-viewer.html';psp2="confNo="+psp['confNo']+'&';break;
		case 'confFrame': f='./conf-frame.html'; psp2="confNo="+psp['confNo']+'&';break;

		case 'maitre':f='./gestConf-maitre.html';break

		case 'body.css':f='./styles//body-html4.css';fileType='text/css';break;
		case 'template1.css':f='./styles//template1.css';fileType='text/css';break;

		default:console.log('----------- chargement de '+f+' ------------------------------------'.debug);
		}

        fs.readFile(f, 'utf-8', function(error, content){
                t="envoie du fichier: '"+f+"'(erreur:"+error+")...";console.log(t.info);
//		res.setEncoding('utf8');
 		res.writeHead(200,
			{
			"Content-Type": fileType
//			,'Set-Cookie': 'node-sessionId='+socket
			});
		//res.writeGet(psp2);	// comment envoyer du get ???
		res.write(content);
		res.end();
		t='le fichier '+f+' a ete envoye';console.log(t.info);
                });
	}; // -- function serveurInit

// -- Chargement de socket.io -- //
console.log('--- chargement de /usr/local/lib/node_modules/socket.io ---'.debug);
var io = require('/usr/local/lib/node_modules/socket.io').listen(server);
console.log('----------------------------'.debug);

//nouvelle connetion recut
io.sockets.on('connection', function (socket)
	{
	GLOBALS['socket']=socket;
	console.log('connection entrante'.info);

	// -- a faire quand un client se connect-- //

	// --- Quand un client se connecte, on lui envoie un message --- //
        socket.emit('connectOk', null);

	// ---  On signale aux autres clients qu'il y a un nouveau venu --- //
	socket.broadcast.emit('console', 'Un autre client vient de se connecter ! ');

	// --- lui envoyer les dernieres donnees mise a jours --- //
	console.log('envoie de la liste des salles'.info);
	socket.emit("setData",{"dataId":"salleList","reset":1,"AR":0,"content":JSON.stringify(gestSalles.datas)});

	console.log('envoie de liste des conferences'.info);
	socket.emit("setData",{"dataId":"confList","reset":1,"AR":0,"content":JSON.stringify(gestConferences.confs)});

	// -- function I/O -- //
	event_init(socket);
	});

// -- function I/O -- //
function event_init(socket){

	// -- COMMUNICATIONS GENERALES -- //
	socket.on('console', function (data){console.log(data.data);});

        // -- COMMUNICATIONS DE DONNEES -- //
        // -- reception d'une mise a jours de fichier -- //
        socket.on('setFichier',function(data)
                {
                console.log('reception d\'uune mise a jours du  fichier: '+data.fichierNom.input);
                switch(data.fichierID){
                        case'':break;
                        }
                });

        // -- reception d'une mise a jours de donnees -- //
       socket.on('getConfUpdate',function(){
		t="reception d'une demande de mise a jours des donnees du serveur a partir des fichiers de donnéees. ";console.log(t.input)
		confsLoad();
		console.log('envoie de liste des conferences'.info);
		socket.emit("setData",{"dataId":"confList","reset":1,"AR":0,"content":JSON.stringify(gestConferences.confs)});
		});
 


	// data.id:id des donnees
	// data.content:contenue des donnees: string,int,ou object
	socket.on('setData',function(data){
		if(!typeof(data)=='object')return null;
		if(!data.dataId)return null;
                t="reception d'une mise a jours de donnee: "+data.dataId;console.log(t.input);
		var ar=(data.ar===1)?1:0;
		switch(data.dataId){
			case'pseudo':
				socket.set('pseudo',data.content, function(){
					t='pseudo mise a jour: '+ data.content;console.log(t.info);
					if(ar)socket.emit('ar','setPseudo');
					})//envoie d'un Accuse reception
				break;
			default:console.log('Aucune donnée concordante.');
			


			// -- reception d'un update des confs en cours par un MAITRE DE CONFERENCE -- //
			// -- action: envoie du tableau gestConfsRun.confsRun aux clients
			// -- //
			case'setconfsRun':
				t="reception d'un update des confs en cours par un MAITRE DE CONFERENCE" ;console.log(t.info);

				// --- verification des donnes --- //
				// aucune: data.content est au format JSON
				t="verification des donnes"+ data.content;console.log(t.info);
				var err=null;
				try{
					json=JSON.parse(data.content);
					}
				catch(e){err=e;}
                                t='envoie auX clientS de l UPDATE de gestConfsRun: ';console.log(t.info);

				if(err===null){
					// --- mise a jours du mirroir local gestConfsRun --- //
					gestConfsRun.confs=data.content;

					// --- envoie auX clients de  gestConfsRun --- //
					t='format JSON compatible envoie de: '+ data.content;console.log(t.data);
                                	socket.broadcast.emit("setData",{"dataId":"setconfsRun","content":data.content});//on envoie les donnees non parser
        				}
				else{
					t="erreur dans le parsage JSON: "+ data.content;console.log(t.warn);
					}			                  		

				break;
			}	// esac

                });
 
	// -- reception d'une demande de donnees -- //
	// data.id:id des donnees
        socket.on('getData',function(data){
                if(!typeof(data)=='object')return null;
                if(!data.dataId)return null;
		t="reception d'une demande de donnee: "+data.dataId;console.log(t.input);
                switch(data.dataId){
			case'pseudo':
				console.log("reception d'une demande de pseudo".input);
				socket.get('pseudo', function(error,pseudo){
					//envoie de la donnee
					t='envoie du pseudo:'+pseudo;console.log(t.info);
					socket.emit('setData',{'dataId':'pseudo','AR':0,'content':pseudo})
					})
				break;

                        case'salleList':
                                console.log("reception d'une demande de liste des salles".input);
                                socket.emit("setData",{"dataId":"salleList","AR":0,"reset":1,"content":JSON.stringify(gestSalles.datas)});
                                break;
			case'confList':
                                console.log("reception d'une demande de liste des conferences".input);
				socket.emit("setData",{"dataId":"confList","AR":0,"reset":1,"content":JSON.stringify(gestConferences.confs)});
                                break;

                        case'confUpdate':
                                console.log("reception d'une demande de mise a jours de la liste des conferences".input);
				confsLoad();
                                socket.emit("setData",{"dataId":"confList","AR":0,"reset":1,"content":JSON.stringify(gestConferences.confs)});
                                break;


			t="reception d'une demande de donnee ( "+data.dataId+"): tag innconnue";console.log(t.input);
                        }

		});
 
        // -- EVENEMENTIELS -- //
	// -- reception de l'information d'une Disponibilite d'un Evenement (EventDispo)
	socket.on('ED',function(dataID){
		if(!dataID)return null;
		switch(data.ID){
			case'a':
				break;
			}	
		});


        socket.on('pokeAll',function(data){
                if(!typeof(data)=='object')return null;
		var pseudo=data.pseudo?data.pseudo:'Inconnu';
		var text=data.text?data.text:pseudo+' vous offre ce Poke';
                //if(!data.id)return null;
                t="relai d'une poke a tous: "+data.ID;console.log(t.info);
		socket.broadcast.emit(text);
		});



 	// -- connection -- //
	// --- un client se connecte --- //
	};	// -- function event_init()

// -- sendFichier: envoie un fichier au client -- //
function sendFichier(fichierId,socket){
	if(!fichierId)return null;	
	if(!socket)return null;

	// -- routage -- //
	var f=null; 
	switch(fichierId){
                case 'sallesDatas':f='./datas/salles.json';break;
		}

	if(!f)return null;
	fs.readFile(f, 'utf-8', function(error, content){
                t="envoie du fichier(erreur:"+error+")): "+f;console.log(t.info);
		GLOBALS['socket'].emit('sendFichier',{'fichierId':fichierID,'content':content});
		
                t="le fichier "+f+" a ete envoye";console.log(t.info);
                });
	}

// -- load/save les donnees -- //
function sallesSave()
	{
	console.log("sauvegarde des salles".info);
	fs.writeFile('./datas/salles.json',JSON.stringify(gestSalles.datas),function(err){
		if(err)console.log('sauvegarde des salles impossibles'.error);
		}); 
	}

function sallesLoad()
        {
	console.log("chargement des salles...".info);
        var fn='./datas/salles.json';
	try{
        	data=fs.readFileSync(fn);
                t="chargement de la liste des salles:"+fn;console.log(t.info);
                var data=fs.readFileSync(fn);
                console.log("salles:".info);
                console.dir(data);
                gestSalles.fullFromRAW(data);      //ajouter les salles 
                }
        catch(err){
                t="ereur à l'ouverture du fichier de salls: "+fn;console.log(t.error);
                }
}

function confsSave()
        {
        console.log("sauvegarde des conferences...".info);
        fs.writeFile('./datas/conferences.json',JSON.stringify(gestConferences.datas),function(err){
                if(err) console.log('sauvegarde des conferences impossibles'.error);
                });

	// --  sauver le contenue des conf -- //

        }

/*
*/

function confsLoad()
        {
        console.log("chargement des conferences...".info);
	var fn='./datas/conferences.json';
	// -- charger les conferences -- //
	var erreur=0;
	var confNb=0;
        var data=null;
	t="chargement de la liste des conferences:"+fn;console.log(t.info);

	try{data=fs.readFileSync(fn);}
	catch(err){
		erreur=1;
                t='erreur: '+err;console.log(t.error);
                t="ereur à l'ouverture du fichier de conference: "+fn;console.log(t.error);
		}
	if(!erreur){
		console.log("contenu du fichier de conferences".info);
		console.log(data.data);
		gestConferences.confs=[];	 // -- reInit du tableau des conf -- //
		confNb=gestConferences.fullFromRAW(data);	//ajouter les conf
		}

	t="nombre de conferences chargées:"+confNb+'/'+gestConferences.confs.length;console.log(t.data);
	console.log("----------------------------".debug);
	// ATTENTION: genere des entree undefined dans le tableau gestConferences.confs -> les supprimer !

	// -- charger les pages des conferences -- //
	console.log("chargement des pages de chaques confs".info);

	for(var confNu in gestConferences.confs){
		 var pageNb=0;
		if(!gestConferences.confs[confNu])continue;
		console.log("chargement des pages pour la conference".info);
		console.dir(gestConferences.confs[confNu]);
		var CNom=gestConferences.confs[confNu].nom;
                var CNomFile='./datas/conferences/'+CNom+'.json';
		t="chargement du fichier: "+CNom;console.log(t.info);

		var erreur=0;
		try{	// --- lire toutes les pages --- //
			data=fs.readFileSync(CNomFile);
			}
		catch(err){
			erreur=1;
			t='erreur: '+err;console.log(t.error);
			var t="la conference "+CNom+" n'a pas de fichier de configuration";
			console.log(t.warn);
			}
		if(!erreur){
			pageNb=gestConferences.confs[confNu].fullFromJSON(data,0,1);
			console.log("Nb de page chargés:");
			}
		console.dir(gestConferences.confs[confNu].pages);
		t="nombre de  pages chargés:"+pageNb+'/'+gestConferences.confs.length;console.log(t.data);
		}
	}


	
// ============================================================================================ //
// -- MAIN -- //

console.log('----------------------------');

