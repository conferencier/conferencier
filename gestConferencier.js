/*
 * projet:conferencier
 * fichier:gestConferences.js
 * auteur: pascal TOLEDO
 * date de creation: 2013.12.14
 * date de modification: 2014.05.03
 * version: 1.0 
 *
 * description: gestion des salles et des conferences via un objet javascript ou un export nodejs
 * dependance: aucune
 *
 */

// -- gestions des salles -- //
function Tsalle(init)
        {
        this.nom=init.nom;
        this.description=init.description?init.description:'';     // description de la salle
        this.confRun=init.confRun?init.confRun:0;         // num (ou titre?) de la conference actuellement en cours
        }


function TgestSalles()
	{
	this.datas=[];
	
        this.fullFromRAW=function(raw,reset){
                if(!raw)return null;
		if(reset)this.datas=[];
                var json=JSON.parse(raw);
                for(i in json) this.add(json[i]);
                }

	this.add=function(init)
		{
		if(!init)return -1;
		if(typeof(init)!='object')return -2;
		if(!init.nom)return -3;
		this.datas[this.datas.length]=new Tsalle(init);
		return this.datas.length;
		}
	return this;
	}


// -- *********************** ** //
// -- gestion des conferences -- //


function TconferencePage(init){
        if(!init.url)return undefined;
        this.url=init.url;
	this.isShow=(init.isShow==0)?0:1;
        this.titre=init.titre?init.titre:'';
	this.description=init.description?init.description:'';
	return this;
        }


function Tconference(init)
	{
	if(!init)return undefined;
        if(typeof(init)!='object')return undefined;
        if(!init.nom)return undefined;

        this.nom=init.nom;
	this.titre=init.titre?init.titre:'sans titre';
        this.description=init.description?init.description:'';
	this.pages=[];

	// -- gestion des pages des conferences-- //

	// --- ajout d'une page--- //
        this.pageAdd=function(init){
//		if(!init)return undefined;
		if(typeof(init)!='object')return undefined;
		if(!init.url)return undefined;
		var pageTotal=this.pages.length;
		this.pages[pageTotal]=new TconferencePage(init);
		return  this.pages[pageTotal];	//renvoie undefined en cas de non ajout
                }

	// --- ajouts de pages d'apres du contenu json --- //
	// --- data donnees json ou brut (raw)--- //
	// --- isStringyfier:les donnes sont stringyfier --- //
        this.fullFromJSON=function(data,reset,isStringyfier){
                if(!data)return null;
                if(reset)this.pages=[];
		var json=data;
		if(isStringyfier)
			{
	                try{json=JSON.parse(data);}			
			catch(e){t="erreur lors du parse des données: "+e;console.log(t.error);}
			}

                var pageNb=0;
		for(var pageNu in json){
                        t="ajout de la page No "+pageNu;console.log(t.info);
			if(this.pageAdd(json[pageNu])){
				t=pageNu+": reussi";console.log(t.info);
				pageNb++;
				}
			else{t=pageNu+": echec";console.log(t.warn);}
			}
                console.log("this.pages");console.dir(this.pages);
                return pageNb;
		}

        // -- ajout des pages a l'appel du constructeur -- //
        if(typeof(init.pages)=='object'){
		console.log("ajout des pages à l'appel du constructeur".info);
		this.fullFromJSON(init.pages,1,0);
                }
	return this;
	}

function TgestConferences()
        {
	this.confs=[];
	this.confAdd=function(init){return (this.confs[this.confs.length]=new Tconference(init));}

	this.fullFromRAW=function(raw,reset){
		if(!raw)return null;
		if(reset)this.confs=[];
		var json=JSON.parse(raw);
		var confNb=0;
		for(var confNu in json) if(this.confAdd(json[confNu]))confNb++;
		return confNb;	// Nb de conf ajoutee
		}
   	return this;
        }

// -- gestionnaire de conferences en cours (piloté par un maitre de conferences) -- // 
function TgestConferencesRun()
	{
	this.confs={};
	this.confs.isRun=[];		// estce que la conference est demmarrer 
	this.confs.pages=[];       // page en cousr de la conf ne index ex .confsRun=[conffNo]=pageNo

	// --specifique au client ne sera pas surcharger lors de la transmittion des vameurs -- //
	this.isFollow=[];	// conference suivi par le client1

	// -- met la conference en etat demarrer (Maitre de conf)-- //
	this.setRun=function(confNu,stop)
                {
                if(isNaN(confNu))return undefined;
                return this.confs.isRun[confNu]=stop?0:1;
                }

	// --- definie la page en cours d'une conf--- //
	this.setPages=function(confNu,pageNu)
		{
		if(isNaN(confNu))return undefined;
		if(isNaN(pageNu))return undefined;
		return this.confs.pages[confNu]=pageNu;
		}

	 // --- tableau a usage des clients: indique si une conf est suivi ou non  --- //
        this.setFollow=function(confNu,notFollow)
                {
                if(isNaN(confNu))return undefined;
                return this.confs.isFollow[confNu]=notFollow?0:1;
		}
	}

// -- activer en tant que module node.js -- //
try{
	exports.gestSalles=new TgestSalles();
	exports.gestConferences=new TgestConferences();
	exports.gestConferencesRun=new TgestConferencesRun();
	}catch(e){}

