/*
 * projet:conferencier
 * fichier:gestSalles-salles.js
 * auteur: pascal TOLEDO
 * date de creation: 2013.12.14
 * date de modification: 2014.05.03
 * version:1.0
 *
 * description:code objet Tsalles et Tconference
 * dependance:aucune
 *
 */

function Tsalles(init)
        {
        this.nom=init.nom;
        this.decription=init.decription?init.description:'';     // description de la salle
        this.confRun=init.confRun?init.confRun:0;         // num de la conference actuellement en cours
        }

function TgestSalles()
	{
	this.salles=[];
	
	this.liste=function()
		{var out='';
		for(var i=0;i<this.salles.length;i++)
			{
			out+='no:'+i+'nom: '+this.salles[i].nom+' decription: '+this.salles[i].decription+' confRun: '+this.salles[i].confRun+'<br>';
			}
		}

	this.salleAdd=function(init)
		{
		if(!init)return -1;
		if(typeof(init)!='object')return -2;
		if(!init.nom)return -3;
		this.salles[this.salles.length]=new Tsalles(init);
		return this.salles.length;
		}
	}

//activer en tant que module node.js
try{exports.gestSalles=new TgestSalles();}catch(e){}

function Tconference()
	{
	this.confNo=0;
	this.confNom='';
	}
